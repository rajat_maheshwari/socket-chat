const message = require('../models/message');
const { CONSTANT } = require('../config/index');
const { addSkipLimit, toObjectId } = require('../util/util');

async function createMessage(roomData, data, userId) {
	try {
		let messageData = {
			'roomId': roomData._id,
			'senderId': userId,
			'message': data.message,
			'status': [{
				'userId': data.receiverId,
				'status': CONSTANT.STATUS.SENT
			}]
		};
		return await message.create(messageData);
	} catch (error) {
		throw error;
	}
}

async function deliverMessage(params) {
	try {
		let query = {};
		query._id = params.messageId;
		query.status = { '$elemMatch': { userId: params.userId, status: CONSTANT.STATUS.SENT } };

		const update = {};
		update['$set'] = {
			'status.$.status': CONSTANT.STATUS.DELIVERED,
			'status.$.deliveredTime': Date.now()
		};
		const options = { new: true, lean: true };

		return await message.findOneAndUpdate(query, update, options);
	} catch (error) {
		throw error;
	}
}

/**
 * @function findAllUnreadMsg
 * @description find all unread and undelivered message
 */
async function findAllUnreadMsg(params) {
	try {
		let query = {};
		if (params.messageId) query._id = params.messageId;
		query.roomId = params.roomId;
		query.senderId = { '$ne': params.userId };
		query.status = { '$elemMatch': { userId: params.userId, status: { '$in': [CONSTANT.STATUS.SENT, CONSTANT.STATUS.DELIVERED] } } };
		const projection = { _id: 1, roomId: 1, message: 1, senderId: 1, created: 1, "status.$": 1, deliveredTime: 1, seenTime: 1 };

		return await message.find(query, projection);
	} catch (error) {
		throw error;
	}
}

async function readMessage(params) {
	try {
		let query = {};
		query._id = params.messageId;
		query.status = { '$elemMatch': { userId: params.userId, status: { '$ne': CONSTANT.STATUS.SEEN } } };

		const update = {};
		update['$set'] = {
			'status.$.status': CONSTANT.STATUS.SEEN,
			'status.$.seenTime': Date.now()
		};
		const options = { new: true, lean: true };

		return await message.findOneAndUpdate(query, update, options);
	} catch (error) {
		throw error;
	}
}

async function messageList(params) {
	try {
		const aggPipe = [];

		let match = {};
		match.roomId = toObjectId(params.roomId);
		aggPipe.push({ '$match': match });

		aggPipe.push({ '$unwind': "$status" });

		aggPipe.push({
			'$project': {
				_id: 1, roomId: 1, message: 1, created: 1, status: '$status.status',
				deliveredTime: '$status.deliveredTime', seenTime: '$status.seenTime'
			}
		});

		aggPipe.push({ '$sort': { created: -1 } });

		// pagination
		aggPipe.push(...addSkipLimit(params.limit, params.pageNo));

		let result = await message.aggregate(aggPipe);

		let nextHit = 0;
		if (result.length > params.limit) {
			nextHit = params.pageNo + 1;
		}
		return {
			'data': result.slice(0, params.limit),
			'total': 0,
			'totalPage': 0,
			'pageNo': params.pageNo,
			'nextHit': nextHit,
			'limit': params.limit
		};
	} catch (error) {
		throw error;
	}
}

module.exports = {
	createMessage,
	deliverMessage,
	findAllUnreadMsg,
	readMessage,
	messageList
};