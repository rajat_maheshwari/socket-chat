const room = require('../models/room');
const { CONSTANT } = require('../config/index');
const { addSkipLimit, toObjectId } = require('../util/util');

/**
 * @function checkIsRoomExists
 * @description to check if both user is already exists in a room or not
 */
async function checkIsRoomExists(params, userId) {
	try {
		let query = {};
		if (params?.roomId) query._id = params.roomId;
		query.groupType = CONSTANT.CHAT.TYPE.DIRECT;
		query.members = { '$all': [userId, params.receiverId] };
		const projection = { _id: 1 };
		return await room.findOne(query, projection);
	} catch (error) {
		throw error;
	}
}

/**
 * @function createRoom
 */
async function createRoom(params) {
	try {
		return await room.create(params);
	} catch (error) {
		throw error;
	}
}

async function findRoomById(roomId) {
	try {
		let query = {};
		query._id = roomId;

		const projection = { updatedAt: 0 };
		const options = { lean: true };

		return await room.findOne(query, projection, options);
	} catch (error) {
		throw error;
	}
}

async function updateLastMsg(roomId, lastMsgId) {
	try {
		let query = {};
		query._id = roomId;

		const update = {};
		update["$set"] = {
			lastMsgId: lastMsgId
		};

		return await room.updateOne(query, update);
	} catch (error) {
		throw error;
	}
}

async function chatList(params, userId) {
	try {
		console.log("sddssssssssssssssssss", params, userId);
		const aggPipe = [];

		let match = {};
		match.members = { '$in': [toObjectId(userId)] };
		match.membersDetail = { '$elemMatch': { userId: toObjectId(userId), isDeleted: false } };
		match.groupType = CONSTANT.CHAT.TYPE.DIRECT;
		aggPipe.push({ '$match': match });

		aggPipe.push({
			'$project': {
				_id: 1,
				lastMsgId: 1,
				membersDetail: {
					'$filter': {
						input: '$membersDetail',
						as: 'item',
						cond: { '$ne': ['$$item.userId', toObjectId(userId)] }
					}
				},
				updatedAt: 1
			}
		});

		aggPipe.push({ '$unwind': '$membersDetail' });

		aggPipe.push({ '$sort': { updatedAt: -1 } });

		// pagination
		aggPipe.push(...addSkipLimit(params.limit, params.pageNo));

		aggPipe.push({
			'$lookup': {
				from: 'users',
				localField: 'membersDetail.userId',
				foreignField: '_id',
				as: 'userId'
			}
		});

		aggPipe.push({ '$unwind': '$userId' });

		aggPipe.push({
			'$project': {
				_id: 1, lastMsgId: 1,
				userId: {
					_id: 1, firstName: 1, lastName: 1, isBlocked: '$membersDetail.isBlocked'
				}
			}
		});

		const lookup1 = {
			from: 'messages',
			let: { messageId: '$lastMsgId' },
			pipeline: [
				{ '$match': { '$expr': { '$eq': ['$_id', '$$messageId'] } } },
				{ '$project': { _id: 1, created: 1, status: 1, message: 1 } },
				{ '$unwind': '$status' }
			],
			as: 'lastMsgInfo'
		};
		aggPipe.push({ '$lookup': lookup1 });

		aggPipe.push({ '$unwind': { path: '$lastMsgInfo', preserveNullAndEmptyArrays: true } });

		const lookup2 = {
			from: 'messages',
			let: { roomId: '$_id' },
			pipeline: [
				{
					'$match': {
						'$expr': { '$eq': ['$roomId', '$$roomId'] },
						senderId: { '$ne': toObjectId(userId) },
						status: { '$elemMatch': { userId: toObjectId(userId), status: { '$in': [CONSTANT.STATUS.SENT, CONSTANT.STATUS.DELIVERED] } } }
					}
				},
				{ '$project': { _id: 1 } }
			],
			as: 'messageData'
		};
		aggPipe.push({ '$lookup': lookup2 });

		aggPipe.push({
			"$project": {
				_id: 1, userId: 1,
				lastMsgInfo: { _id: 1, message: 1, created: 1, status: '$lastMsgInfo.status.status' },
				unReadMsgCount: { '$cond': { if: { '$isArray': '$messageData' }, then: { '$size': '$messageData' }, else: 0 } }
			}
		});

		let result = await room.aggregate(aggPipe);

		let nextHit = 0;
		if (result.length > params.limit) {
			nextHit = params.pageNo + 1;
		}
		return {
			'data': result.slice(0, params.limit),
			'total': 0,
			'totalPage': 0,
			'pageNo': params.pageNo,
			'nextHit': nextHit,
			'limit': params.limit
		};
	} catch (error) {
		throw error;
	}
}

module.exports = {
	checkIsRoomExists,
	createRoom,
	findRoomById,
	updateLastMsg,
	chatList
};