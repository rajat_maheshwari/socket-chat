const authMiddleware = require('../util/authMiddleware');
const messageValidator = require('../util/messageValidator');
const messageRoute = require('express').Router();
const messageController = require('../controllers/message');

messageRoute.route('/').get([authMiddleware.bearerAuth, messageValidator.messageList], function (req, res) {
	messageController.messageList(req, res);
});

module.exports = messageRoute;