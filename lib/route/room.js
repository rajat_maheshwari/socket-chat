const authMiddleware = require('../util/authMiddleware');
const roomValidator = require('../util/roomValidator');
const roomRoute = require('express').Router();
const roomController = require('../controllers/room');

roomRoute.route('/').get([authMiddleware.bearerAuth, roomValidator.chatList], function (req, res) {
	roomController.chatList(req, res);
});

module.exports = roomRoute;