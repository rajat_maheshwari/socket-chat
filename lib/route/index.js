const roomRoute = require('./room');
const messageRoute = require('./message');
const userRoute = require('./user');

module.exports = function (app) {
	app.use(function (request, response, next) {
		console.log('--------------------------------REQUEST STARTS----------------------------------------');
		console.log(request.originalUrl);
		console.log('Request Address=======>', request.headers['x-forwarded-for'] || request.connection.remoteAddress);
		console.log('Request Type=======>', request.method.toUpperCase());
		console.log('Request Path=======>', request.path);
		console.log('Request Body=======>', request.body);
		console.log('Request Params=====>', request.params);
		console.log('Request Query======>', request.query);
		console.log('Authorization======>', request.get('authorization'));
		console.log('--------------------------------REQUEST ENDS------------------------------------------');

		if (request.method === 'OPTIONS') {
			response.send(200);
		} else {
			next();
		}
	});

	// Attach room Routes
	app.use('/v1/chat', roomRoute);
	// Attach message Routes
	app.use('/v1/message', messageRoute);
	// Attach user Routes
	app.use('/v1/user', userRoute);
};