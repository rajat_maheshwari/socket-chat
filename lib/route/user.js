const authMiddleware = require('../util/authMiddleware');
const userValidator = require('../util/userValidator');
const userRoute = require('express').Router();
const userController = require('../controllers/user');

userRoute.route('/').post([authMiddleware.basicAuth, userValidator.createUser], function (req, res) {
	userController.createUser(req, res);
});

userRoute.route('/login').post([authMiddleware.basicAuth, userValidator.loginUser], function (req, res) {
	userController.loginUser(req, res);
});

userRoute.route('/details').get([authMiddleware.bearerAuth], function (req, res) {
	userController.userDetails(req, res);
});

userRoute.route('/logout').post([authMiddleware.bearerAuth], function (req, res) {
	userController.userLogout(req, res);
});

module.exports = userRoute;