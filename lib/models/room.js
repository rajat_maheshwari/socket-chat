const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { CONSTANT } = require('../config/index');

const roomSchema = new Schema({
	userId: { // created by
		type: Schema.Types.ObjectId,
		ref: CONSTANT.DB_MODEL_REF.USER
	},
	groupName: { type: String, default: "" },
	groupImage: { type: String, default: "" },
	groupType: {
		type: String,
		enum: [
			CONSTANT.CHAT.TYPE.DIRECT,
			CONSTANT.CHAT.TYPE.GROUP
		]
	},
	members: [{ // members info
		type: Schema.Types.ObjectId,
		ref: CONSTANT.DB_MODEL_REF.USER
	}],
	membersDetail: [{
		_id: false,
		userId: {
			type: Schema.Types.ObjectId,
			ref: CONSTANT.DB_MODEL_REF.USER,
			index: true
		},
		joinedAt: { type: Number, required: true },
		isDeleted: { type: Boolean, default: false }, // when user will be removed from group
		isBlocked: { type: Boolean, default: false },
		isOwner: { type: Boolean, default: false },
		isAdmin: { type: Boolean, default: false },
		isMember: { type: Boolean, default: true }
	}],
	lastMsgId: {
		type: Schema.Types.ObjectId,
		ref: CONSTANT.DB_MODEL_REF.MESSAGE,
		default: null
	},
	created: { type: Number, default: Date.now }
}, {
	timestamps: true,
	versionKey: false
});

roomSchema.index({ userId: 1 });
roomSchema.index({ groupName: 1 });
roomSchema.index({ groupType: 1 });
roomSchema.index({ members: 1 });
roomSchema.index({ "membersDetail.userId": 1, "membersDetail.isDeleted": 1 });
roomSchema.index({ created: -1 });
roomSchema.index({ updatedAt: -1 });

module.exports = mongoose.model(CONSTANT.DB_MODEL_REF.ROOM, roomSchema);