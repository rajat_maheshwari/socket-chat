const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { encryptHashPassword, genRandomString } = require('../util/util');
const { CONSTANT, SERVER } = require('../config/index');

const userSchema = new Schema({
	firstName: { type: String, required: true },
	lastName: { type: String, required: false },
	email: { type: String, required: true, unique: true },
	salt: { type: String, required: true },
	hash: { type: String, required: true },
	socketId: { type: String, required: false },
	lastSeen: { type: Number, default: 0 },
	isOnline: { type: Boolean, default: false },
	created: { type: Number, default: Date.now }
}, {
	timestamps: true,
	versionKey: false
});

// Load password virtually
userSchema.virtual('password')
	.get(function () {
		return this._password;
	})
	.set(function (password) {
		this._password = password;
		const salt = this.salt = genRandomString(SERVER.SALT_ROUNDS);
		this.hash = encryptHashPassword(password, salt);
	});

userSchema.index({ firstName: 1 });
userSchema.index({ lastName: 1 });
userSchema.index({ email: 1 });
userSchema.index({ created: -1 });

module.exports = mongoose.model(CONSTANT.DB_MODEL_REF.USER, userSchema);