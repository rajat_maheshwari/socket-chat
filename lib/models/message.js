const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { CONSTANT } = require('../config/index');

const messageSchema = new Schema({
	roomId: {
		type: Schema.Types.ObjectId,
		ref: CONSTANT.DB_MODEL_REF.ROOM
	},
	senderId: {
		type: Schema.Types.ObjectId,
		ref: CONSTANT.DB_MODEL_REF.USER
	},
	message: { type: String, default: "" },
	status: [{
		_id: false,
		userId: {
			type: Schema.Types.ObjectId,
			ref: CONSTANT.DB_MODEL_REF.USER
		},
		status: {
			type: String,
			enum: [
				CONSTANT.STATUS.SENT,
				CONSTANT.STATUS.DELIVERED,
				CONSTANT.STATUS.SEEN
			],
			default: CONSTANT.STATUS.SENT
		},
		deliveredTime: { type: Number },
		seenTime: { type: Number }
	}],
	created: { type: Number, default: Date.now }
}, {
	timestamps: true,
	versionKey: false
});

messageSchema.index({ roomId: 1 });
messageSchema.index({ senderId: 1 });
messageSchema.index({ "status.userId": 1, "status.status": 1 });
messageSchema.index({ created: -1 });

module.exports = mongoose.model(CONSTANT.DB_MODEL_REF.MESSAGE, messageSchema);