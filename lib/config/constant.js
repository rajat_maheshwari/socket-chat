const DB_MODEL_REF = {
	MESSAGE: 'messages',
	ROOM: 'rooms',
	USER: 'users'
};

const REGEX = {
	EMAIL: /^\w+([.-]\w+)*@\w+([.-]\w+)*\.\w{2,5}$/i,
	MONGO_ID: /^[a-f\d]{24}$/i
};

const STATUS = {
	SENT: 'SENT',
	DELIVERED: 'DELIVERED',
	SEEN: 'SEEN'
};

const CHAT = {
	TYPE: {
		DIRECT: 'DIRECT',
		GROUP: 'GROUP'
	}
};

let SOCKET = {
	ERROR: {
		BAD_TOKEN: {
			'statusCode': 401,
			'message': 'Token is not valid.',
			'type': 'BAD_TOKEN',
			'data': {}
		},
		NETWORK: (error) => {
			const errorMessage = typeof error === "object" ? error.message : error;
			return {
				'statusCode': 400,
				'message': errorMessage,
				'type': 'NETWORK',
				'data': {}
			};
		},
		SOCKET: {
			'statusCode': 400,
			'message': 'Socket Implementation error.',
			'type': 'SOCKET_ERROR',
			'data': {}
		},
		INFO_MISSING: (message) => {
			return {
				'statusCode': 400,
				'message': message ? message : 'Some required information is missing.',
				'type': 'INFO_MISSING',
				'data': {}
			};
		},
		ERROR: (message) => {
			return {
				'statusCode': 400,
				'message': message,
				'type': 'ERROR',
				'data': {}
			};
		}
	},
	SUCCESS: {
		DEFAULT: (data) => {
			return {
				'statusCode': 200,
				'message': 'Default',
				'data': data
			};
		},
		CONNECTION_ESTABLISHED: {
			'statusCode': 200,
			'message': 'Connection Established',
			'data': {}
		},
		ACKNOWLEDGED: (data) => {
			return {
				"statusCode": 200,
				"message": "Successfully acknowledged on server",
				"type": "ACKNOWLEDGED",
				"data": data
			}
		}
	},
	EVENT: {
		EMITTER: {
			PING: 'PING',
			ERROR: {
				SOCKET_ERROR: 'socket-error',
				AUTHORIZATION_ERROR: 'authorization-error',
				NETWORK_ERROR: 'network-error',
				INSUFFICIENT_INFO_ERROR: 'insufficient-info-error'
			},
			DEFAULT: {
				CONNECTED: 'connected'
			},
			CHAT: {
				RECEIVE_MESSAGE: 'receive-message',
				DELIVER_MESSAGE_ACK: 'deliver-message-ack',
				READ_MESSAGE_ACK: 'read-message-ack'
			}
		},
		LISTENER: {
			SOCKET_SERVICE: 'socket-service',
			DEFAULT: {
				CONNECTION: 'connection',
				RECONNECT: 'reconnect',
				DISCONNECT: 'disconnect'
			}
		},
		LISTNER_TYPE: {
			CHAT: 'chat'
		},
		LISTNER_ACTION: {
			CHAT: {
				SEND_MESSAGE: 'send-message',
				DELIVER_MESSAGE: 'deliver-message',
				READ_MESSAGE: 'read-message'
			}
		}
	}
};

const CONSTANT = Object.freeze({
	DB_MODEL_REF,
	REGEX,
	STATUS,
	CHAT,
	SOCKET
});

module.exports = CONSTANT;