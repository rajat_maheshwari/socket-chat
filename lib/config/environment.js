const dotenv = require('dotenv');
const path = require('path');

dotenv.config({ path: '.env' });

const SERVER = Object.freeze({
	PORT: process.env['PORT'],
	MONGO: {
		DB_NAME: process.env['DB_NAME'],
		DB_URL: process.env['DB_URL'],
		OPTIONS: {
			// user: process.env['DB_USER'],
			// pass: process.env['DB_PASSWORD'],
			useNewUrlParser: true,
			useCreateIndex: true,
			useUnifiedTopology: true,
			useFindAndModify: false
		}
	},
	BASIC_AUTH: {
		NAME: '12345',
		PASS: '12345'
	},
	REDIS: {
		HOST: process.env["REDIS_HOST"],
		PORT: process.env["REDIS_PORT"],
		DB: process.env["REDIS_DB"]
	},
	SOCKET: {
		PORT: process.env["SOCKET_PORT"],
		HOST: process.env["SOCKET_HOST"]
	},
	jwtSecretKey: 'g8b9(-=~Sdf)',
	SALT_ROUNDS: 10
});

module.exports = SERVER;