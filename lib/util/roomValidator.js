const Joi = require('joi');
const apiResponder = require('./responseHandler').apiResponder;

function chatList(req, res, next) {
	try {
		const schema = Joi.object({
			pageNo: Joi.number().required(),
			limit: Joi.number().required()
		});

		const validation = schema.validate(req.query);

		if (validation?.error?.details?.length) throw new Error(validation.error.details[0].message.replace(/"/g, ""));
		next();
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

module.exports = {
	chatList
};