const crypto = require('crypto');
const mongoose = require('mongoose');

const { CONSTANT, SERVER } = require('../config/index');

const toObjectId = function (id) {
	return mongoose.Types.ObjectId(id);
};

const basicAuthFunction = function (token) {
	if (token.split(' ').length !== 2) return false;
	const credentials = Buffer.from(token.split(' ')[1], 'base64').toString('ascii');
	const [username, password] = credentials.split(':');
	if (username !== SERVER.BASIC_AUTH.NAME || password !== SERVER.BASIC_AUTH.PASS) { return false; }
	return true;
};

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
*/
const genRandomString = function (length) {
	return crypto.randomBytes(Math.ceil(length / 2))
		.toString('hex') /** convert to hexadecimal format */
		.slice(0, length);   /** return required number of characters */
};

const encryptHashPassword = function (password, salt) {
	const hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
	hash.update(password);
	return hash.digest('hex');
};

const matchPassword = function (password, dbHash, salt) {
	if (!salt) return false;
	const hash = encryptHashPassword(password, salt);
	if (dbHash !== hash) {
		return false;
	} else
		return true;
};

const socketErrorHandler = function (socket, error) {
	console.log('socketErrorHandler', error);
	if (error.type == 'INFO_MISSING') {
		socket.emit(CONSTANT.SOCKET.EVENT.EMITTER.ERROR.INSUFFICIENT_INFO_ERROR, error);
	} else {
		socket.emit(CONSTANT.SOCKET.EVENT.EMITTER.ERROR.NETWORK_ERROR, CONSTANT.SOCKET.ERROR.NETWORK(error));
	}
	return {};
};

const generateMsgPayload = function (data) {
	return {
		'messageId': data._id,
		'roomId': data.roomId,
		'message': data.message,
		'created': data.created,
		'status': data.status[0].status,
		'senderId': data.senderId,
		'deliveredTime': data.status[0]?.deliveredTime,
		'seenTime': data.status[0]?.seenTime
	};
};

/**
 * @description Add skip and limit to pipleine
 */
const addSkipLimit = function (limit, pageNo) {
	if (limit) {
		limit = Math.abs(limit);
		// If limit exceeds max limit
		if (limit > 100) {
			limit = 100;
		}
	} else {
		limit = 10;
	}
	if (pageNo && (pageNo !== 0)) {
		pageNo = Math.abs(pageNo);
	} else {
		pageNo = 1;
	}
	let skip = (limit * (pageNo - 1));
	return [
		{ '$skip': skip },
		{ '$limit': limit + 1 }
	];
}

module.exports = {
	deleteFiles,
	toObjectId,
	basicAuthFunction,
	genRandomString,
	encryptHashPassword,
	matchPassword,
	socketErrorHandler,
	generateMsgPayload,
	addSkipLimit
};