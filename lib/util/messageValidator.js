const Joi = require('joi');
const apiResponder = require('./responseHandler').apiResponder;

const { CONSTANT } = require('../config/index');

function messageList(req, res, next) {
	try {
		const schema = Joi.object({
			pageNo: Joi.number().required(),
			limit: Joi.number().required(),
			roomId: Joi.string().trim().regex(CONSTANT.REGEX.MONGO_ID).required()
		});

		const validation = schema.validate(req.query);

		if (validation?.error?.details?.length) throw new Error(validation.error.details[0].message.replace(/"/g, ""));
		next();
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

module.exports = {
	messageList
};