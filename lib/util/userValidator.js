const Joi = require('joi');
const apiResponder = require('./responseHandler').apiResponder;

const { CONSTANT } = require('../config/index');

function createUser(req, res, next) {
	try {
		const schema = Joi.object({
			firstName: Joi.string().trim().required(),
			lastName: Joi.string().trim().optional(),
			email: Joi.string()
				.trim()
				.lowercase()
				.email({ minDomainSegments: 2 })
				.regex(CONSTANT.REGEX.EMAIL)
				.required(),
			password: Joi.string().trim().min(5).max(30).required()
		});

		const validation = schema.validate(req.body);

		if (validation?.error?.details?.length) throw new Error(validation.error.details[0].message.replace(/"/g, ""));
		next();
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

function loginUser(req, res, next) {
	try {
		const schema = Joi.object({
			email: Joi.string()
				.trim()
				.lowercase()
				.email({ minDomainSegments: 2 })
				.regex(CONSTANT.REGEX.EMAIL)
				.required(),
			password: Joi.string().trim().min(5).max(30).required()
		});

		const validation = schema.validate(req.body);

		if (validation?.error?.details?.length) throw new Error(validation.error.details[0].message.replace(/"/g, ""));
		next();
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

module.exports = {
	createUser,
	loginUser
};