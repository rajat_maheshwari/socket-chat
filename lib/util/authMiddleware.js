const _ = require('lodash');

const apiResponder = require('./responseHandler').apiResponder;
const { basicAuthFunction } = require('../util/util');
const jwtService = require('../util/jwtService');
const userEntity = require('../entity/user');
const redisClient = require('../util/redisClient');
const { CONSTANT } = require('../config/index');

function basicAuth(req, res, next) {
	try {
		if (!req.get('authorization')) throw new Error('basic auth token is required.');

		const token = req.get('authorization');

		const isValid = basicAuthFunction(token);
		if (!isValid) throw new Error("Token is invalid!");
		next();
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

const bearerAuth = async function (req, res, next) {
	try {
		if (!req.get('authorization')) throw new Error('bearer auth token is required.');

		const token = req.get('authorization').split(' ')[1];
		const tokenData = jwtService.verifyToken(token);
		const isExist = await userEntity.findUserById(tokenData.userId);
		if (!isExist) throw new Error('Token is invalid!');
		const isSession = await redisClient.getValue(tokenData.userId);
		if (!isSession) throw new Error('Your login session has been expired.');
		req.user = (_.pick({ ...tokenData, ...isExist }, 'userId', 'firstName', 'lastName', 'email'));
		next();
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

const verifySocketToken = async function (socket, next) {
	try {
		if (!socket.handshake.query || !socket.handshake.query.authorization) next(new Error('Token not found.'));

		const token = socket.handshake.query.authorization;

		const tokenData = jwtService.verifyToken(token);
		const isExist = await userEntity.findUserById(tokenData.userId);
		if (!isExist) next(new Error('Token is invalid!'));
		const isSession = await redisClient.getValue(tokenData.userId);
		if (!isSession) throw new Error('Your login session has been expired.');
		socket.user = (_.pick({ ...tokenData, ...isExist }, 'userId', 'firstName', 'lastName', 'email'));
		next();
	} catch (error) {
		socket.emit(CONSTANT.SOCKET.EVENT.EMITTER.ERROR.AUTHORIZATION_ERROR, CONSTANT.SOCKET.ERROR.BAD_TOKEN);
	}
}

module.exports = {
	basicAuth,
	bearerAuth,
	verifySocketToken
};