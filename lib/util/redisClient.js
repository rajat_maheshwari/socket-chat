const redis = require('redis');
const { promisify } = require('util');

const { SERVER } = require('../config/index');

var client;

const init = function () {
	const CONF = { db: SERVER.REDIS.DB };
	client = redis.createClient(SERVER.REDIS.PORT, SERVER.REDIS.HOST, CONF, { disable_resubscribing: true });
	client.on("ready", () => {
		console.log(`Redis server listening on ${SERVER.REDIS.HOST}:${SERVER.REDIS.PORT}, in ${SERVER.REDIS.DB} DB`);
	});
	client.on('error', (error) => {
		console.log('Error in Redis', error);
	});
}
init();

const storeValue = async function (key, value) {
	try {
		const promise = promisify(client.set).bind(client);
		await promise(key.toString(), value.toString());
		return {};
	} catch (error) {
		return Promise.reject(error);
	}
}

const getValue = async function (key) {
	try {
		const promise = promisify(client.get).bind(client);
		const value = await promise(key.toString());
		return value;
	} catch (error) {
		return Promise.reject(error);
	}
}

function deleteKey(key) {
	return client.del(key, function (error, reply) {
		if (error) {
			console.log(error);
		}
		return reply;
	});
}

module.exports = {
	storeValue,
	getValue,
	deleteKey
};