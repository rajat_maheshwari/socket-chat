module.exports = {
	apiResponder: (res, responseMessage, responseCode, data) => {
		return res.json({
			code: responseCode || 400,
			message: responseMessage || 'Something went wrong!',
			data: data || {}
		});
	}
}