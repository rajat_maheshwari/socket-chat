const Joi = require('joi');

const { CONSTANT } = require('../../config/index');

const socketValidator = async function (data) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object({
			type: Joi.string().valid(
				CONSTANT.SOCKET.EVENT.LISTNER_TYPE.CHAT
			).required(),
			actionType: Joi.string().valid(
				CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.SEND_MESSAGE,
				CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.DELIVER_MESSAGE,
				CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.READ_MESSAGE
			).required(),
			data: Joi.any().required()
		});

		const validation = schema.validate(data);

		if (validation?.error?.details?.length) reject(CONSTANT.SOCKET.ERROR.INFO_MISSING(validation.error.details[0].message.replace(/"/g, "")));
		resolve({});
	});
}

const sendMessageValidator = async function (data) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object({
			roomId: Joi.string().trim().regex(CONSTANT.REGEX.MONGO_ID).optional(),
			receiverId: Joi.string().trim().regex(CONSTANT.REGEX.MONGO_ID).required(),
			message: Joi.string().trim().required()
		});

		const validation = schema.validate(data);

		if (validation?.error?.details?.length) reject(CONSTANT.SOCKET.ERROR.INFO_MISSING(validation.error.details[0].message.replace(/"/g, "")));
		resolve({});
	});
}

const deliverMessageValidator = async function (data) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object({
			messageId: Joi.string().trim().regex(CONSTANT.REGEX.MONGO_ID).required()
		});

		const validation = schema.validate(data);

		if (validation?.error?.details?.length) reject(CONSTANT.SOCKET.ERROR.INFO_MISSING(validation.error.details[0].message.replace(/"/g, "")));
		resolve({});
	});
}

const readMessageValidator = async function (data) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object({
			roomId: Joi.string().trim().regex(CONSTANT.REGEX.MONGO_ID).required(),
			messageId: Joi.string().trim().regex(CONSTANT.REGEX.MONGO_ID).optional()
		});

		const validation = schema.validate(data);

		if (validation?.error?.details?.length) reject(CONSTANT.SOCKET.ERROR.INFO_MISSING(validation.error.details[0].message.replace(/"/g, "")));
		resolve({});
	});
}

module.exports = {
	socketValidator,
	sendMessageValidator,
	deliverMessageValidator,
	readMessageValidator
};