const express = require('express'),
	http = require('http'),
	socketio = require('socket.io');
const { promisify } = require('util');

const { SERVER, CONSTANT } = require('../../config/index');
const { verifySocketToken } = require('../authMiddleware');
const userEntity = require('../../entity/user');
const socketValidator = require('./socketValidator');
const chatSocket = require('./chatSocket');
const redisClient = require('../redisClient');
const { socketErrorHandler } = require('../util');

const app = express();
const server = http.createServer(app);
server.listen(SERVER.SOCKET.PORT, { 'pingTimeout': 4000, 'pingInterval': 2000 });

const io = socketio.listen(server, {
	transports: ['polling', 'websocket']
});

// socket access token authenticate
io.use(verifySocketToken);

let connections = [];

io.sockets.on(CONSTANT.SOCKET.EVENT.LISTENER.DEFAULT.CONNECTION, function (socket) {
	socketReconnect(socket);
	console.log('connection established');
	console.log('socket.id', socket.id);

	if (socket.user) {
		connections.push(socket);
		console.log(' %s sockets is connected', connections.length);
		socket.user.socketId = socket.id;
		// socket handlers
		socketConnections(socket);
		addSocketHandler(io, socket);
		socketDisconnections(io, socket);
	}
});

function socketReconnect(socket) {
	socket.on(CONSTANT.SOCKET.EVENT.LISTENER.DEFAULT.RECONNECT, function () {
		// Avoid that buffered emits getting send again
		console.log('CLEAR SOCKET BUFFER');
		socket.sendBuffer = [];
	});
	return {};
}

const socketConnections = async function (socket) {
	console.log(socket.user.userId + " user has this socket id " + socket.id);
	socket.emit(CONSTANT.SOCKET.EVENT.EMITTER.DEFAULT.CONNECTED, CONSTANT.SOCKET.SUCCESS.CONNECTION_ESTABLISHED);
	await waitingJoin(socket, [socket.user.userId]);
	console.log(io.sockets.adapter.rooms);
	let userData = await redisClient.getValue(socket.user.userId);
	userData = JSON.parse(userData);
	await redisClient.storeValue(socket.user.userId, JSON.stringify({ ...userData, 'socketId': socket.id }));
	await userEntity.addSocketId({ 'userId': socket.user.userId, 'socketId': socket.id });
}

function addSocketHandler(io, socket) {
	socket.on(CONSTANT.SOCKET.EVENT.LISTENER.SOCKET_SERVICE, async (data, ack) => {
		try {
			await socketValidator.socketValidator(data);
			switch (data.type) {
				case CONSTANT.SOCKET.EVENT.LISTNER_TYPE.CHAT: {
					await chatSocket.chatHandlers(io, socket, data, ack, socket.user);
					break;
				}
				default: {
					console.log('addSocketHandler', JSON.stringify(data));
					socket.emit(CONSTANT.SOCKET.EVENT.EMITTER.ERROR.SOCKET_ERROR, CONSTANT.SOCKET.ERROR.SOCKET);
					break;
				}
			}
		} catch (error) {
			socketErrorHandler(socket, error);
		}
	});
}

function socketDisconnections(io, socket) {
	try {
		socket.on(CONSTANT.SOCKET.EVENT.LISTENER.DEFAULT.DISCONNECT, function () {
			socketCheckOnDisconnect(io, socket, socket.user.userId);
		});
	} catch (error) {
		console.log('socketDisconnections', error);
		return Promise.reject(error);
	}
}

function socketCheckOnDisconnect(io, socket, userId) {
	try {
		let socketDisconnectTimer = setTimeout(async () => {
			console.log('connection disconnected', userId, socket.id);
			connections.splice(connections.indexOf(socket), 1);
			console.log(' %s socket connection(s) remaining after disconnect', connections.length);
			socket.leave(userId);
			console.log(io.sockets.adapter.rooms);
			let userData = await redisClient.getValue(userId);
			userData = JSON.parse(userData);
			delete userData.socketId;
			await redisClient.storeValue(socket.user.userId, JSON.stringify(userData));
			await userEntity.removeSocketId({ "userId": userId, "socketId": socket.id });
		}, 5000);

		socket.emit(CONSTANT.SOCKET.EVENT.EMITTER.PING, 'PING', function (ack) {
			console.log('PING', [userId, ack]);
			if (ack) {
				clearTimeout(socketDisconnectTimer);
				console.log('fake disconnect', 'Fake disconnect call');
				return {};
			} else {
				return {};
			}
		});
	} catch (error) {
		console.log('socketCheckOnDisconnect', error);
		return Promise.reject(error);
	}
}

const waitingJoin = async function (socket, rooms) {
	try {
		const promise = promisify(socket.join).bind(socket);
		return await promise(rooms);
	} catch (error) {
		console.log('waitingJoin', error);
		return Promise.reject(error);
	}
}