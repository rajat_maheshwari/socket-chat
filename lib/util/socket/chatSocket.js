const { CONSTANT } = require('../../config/index');
const socketValidator = require('./socketValidator');
const { generateMsgPayload, socketErrorHandler } = require('../util');
const userEntity = require('../../entity/user');
const roomEntity = require('../../entity/room');
const messageEntity = require('../../entity/message');
const redisClient = require('../redisClient');

function chatHandlers(io, socket, data, ack, userData) {
	switch (data.actionType) {
		/*{
			"type": "chat",
			"actionType": "send-message",
			"data": {
				"roomId": "",
				"receiverId": "",
				"message": "hiii"
			}
		};*/
		case CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.SEND_MESSAGE: {
			console.log(CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.SEND_MESSAGE, JSON.stringify(data.data));
			sendMessage(io, socket, data.data, ack, userData);
			break;
		}
		/*{
			"type": "chat",
			"actionType": "deliver-message",
			"data": {
				"messageId": ""
			}
		};*/
		case CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.DELIVER_MESSAGE: {
			console.log(CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.DELIVER_MESSAGE, JSON.stringify(data.data));
			deliverMessage(io, socket, data.data, ack, userData);
			break;
		}
		/*{
			"type": "chat",
			"actionType": "read-message",
			"data": {
				"roomId": "",
				"messageId": ""
			}
		};*/
		case CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.READ_MESSAGE: {
			console.log(CONSTANT.SOCKET.EVENT.LISTNER_ACTION.CHAT.READ_MESSAGE, JSON.stringify(data.data));
			readMessage(io, socket, data.data, ack, userData);
			break;
		}
	}
	return {};
}

/**
 * @description emit to message receiver
 */
const sendMessage = async function (io, socket, data, ack, userData) {
	try {
		await socketValidator.sendMessageValidator(data);

		const isReceiverExist = await userEntity.findUserById(data.receiverId);
		// @comment : check receiver id is valid
		if (!isReceiverExist) throw CONSTANT.SOCKET.ERROR.ERROR("User does not exists");

		let chatThread = {};
		if (!data.roomId) { // @comment : new chat creation for direct chat when the user initiate chat with other user
			const isRoomExist = await roomEntity.checkIsRoomExists(data, userData.userId);

			if (!isRoomExist) {
				const roomData = {
					'userId': userData.userId,
					'groupType': CONSTANT.CHAT.TYPE.DIRECT,
					'members': [userData.userId, data.receiverId],
					'membersDetail': [{
						'userId': userData.userId,
						'joinedAt': Date.now(),
						'isOwner': true,
						'isAdmin': true
					}, {
						'userId': data.receiverId,
						'joinedAt': Date.now(),
						'isOwner': false,
						'isAdmin': false
					}]
				};
				chatThread = await roomEntity.createRoom(roomData);
			} else {
				chatThread = isRoomExist;
			}
		} else {
			chatThread = await roomEntity.findRoomById(data.roomId);
		}

		// @comment : insert message to message collection & save last message id in to room collection
		const msgThread = await messageEntity.createMessage(chatThread, data, userData.userId);
		msgThread.id = data.id;
		await roomEntity.updateLastMsg(chatThread._id, msgThread._id);

		// @comment : generate payload to be send
		const messagePayload = generateMsgPayload(msgThread);

		// @comment : send message to receiver
		let redisData = await redisClient.getValue(data.receiverId);
		redisData = JSON.parse(redisData);
		const socketId = redisData.socketId;
		if (socketId && socketId !== "" && io.sockets.connected[socketId]) {
			console.log("sendMessageStatus========================>", CONSTANT.SOCKET.EVENT.EMITTER.CHAT.RECEIVE_MESSAGE);
			io.in(data.receiverId).emit(CONSTANT.SOCKET.EVENT.EMITTER.CHAT.RECEIVE_MESSAGE, CONSTANT.SOCKET.SUCCESS.DEFAULT(messagePayload));
		}

		if (ack || ack !== undefined) ack(CONSTANT.SOCKET.SUCCESS.DEFAULT(messagePayload));
	} catch (error) {
		socketErrorHandler(socket, error);
	}
}

/**
 * @description emit to message sender
 */
const deliverMessage = async function (io, socket, data, ack, userData) {
	try {
		await socketValidator.deliverMessageValidator(data);
		const messageInfo = await messageEntity.deliverMessage({ ...data, 'userId': userData.userId });

		// @comment : generate payload to be send
		const messagePayload = generateMsgPayload(messageInfo);

		// @comment : receive message ack to sender
		let redisData = await redisClient.getValue(messagePayload.senderId.toString());
		redisData = JSON.parse(redisData);
		const socketId = redisData.socketId;
		if (socketId && socketId !== "" && io.sockets.connected[socketId]) {
			console.log("deliverMessage========================>", CONSTANT.SOCKET.EVENT.EMITTER.CHAT.DELIVER_MESSAGE_ACK);
			io.in(messagePayload.senderId.toString()).emit(CONSTANT.SOCKET.EVENT.EMITTER.CHAT.DELIVER_MESSAGE_ACK, CONSTANT.SOCKET.SUCCESS.DEFAULT(messagePayload));
		}

		if (ack || ack !== undefined) ack(SOCKET.SUCCESS.DEFAULT(messagePayload));
	} catch (error) {
		socketErrorHandler(socket, error);
	}
}

/**
 * @description emit to message sender
 */
const readMessage = async function (io, socket, data, ack, userData) {
	try {
		await socketValidator.readMessageValidator(data);
		const messages = await messageEntity.findAllUnreadMsg({ ...data, 'userId': userData.userId });

		// @comment : get sender data from redis
		let socketId;
		if (messages.length) {
			let redisData = await redisClient.getValue(messages[0].senderId.toString());
			redisData = JSON.parse(redisData);
			socketId = redisData.socketId;
		}

		for await (let v of messages) {
			if (v.status[0].status === CONSTANT.STATUS.SENT) await messageEntity.deliverMessage({ 'messageId': v._id, 'userId': userData.userId });
			const messageInfo = await messageEntity.readMessage({ "messageId": v._id, 'userId': userData.userId });

			// @comment : generate payload to be send
			const messagePayload = generateMsgPayload(messageInfo);

			// @comment : send read message ack to sender
			if (socketId && socketId !== "" && io.sockets.connected[socketId]) {
				console.log("readMessage========================>", CONSTANT.SOCKET.EVENT.EMITTER.CHAT.READ_MESSAGE_ACK);
				io.in(v.senderId.toString()).emit(CONSTANT.SOCKET.EVENT.EMITTER.CHAT.READ_MESSAGE_ACK, CONSTANT.SOCKET.SUCCESS.DEFAULT(messagePayload));
			}
		}

		if (ack || ack !== undefined) ack(SOCKET.SUCCESS.ACKNOWLEDGED({}));
	} catch (error) {
		socketErrorHandler(socket, error);
	}
}

module.exports = {
	chatHandlers
};