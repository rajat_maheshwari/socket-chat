const apiResponder = require('../util/responseHandler').apiResponder;
const messageEntity = require('../entity/message');

/**
 * @function messageList
 * @description to list all message
 * @author Rajat Maheshwari
 * @param req.query.pageNo: given page no (required)
 * @param req.query.limit: given limit (required)
 */
async function messageList(req, res) {
	try {
		const data = await messageEntity.messageList(req.query);
		return apiResponder(res, 'Message list get successfully!', 200, data);
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

module.exports = {
	messageList
};