const _ = require('lodash');

const apiResponder = require('../util/responseHandler').apiResponder;
const userEntity = require('../entity/user');
const { matchPassword } = require('../util/util');
const jwtService = require('../util/jwtService');
const redisClient = require('../util/redisClient');

/**
 * @function createUser
 * @description to create an user
 * @author Rajat Maheshwari
 * @param req.body.firstName: user's first name (required)
 * @param req.body.lastName: user's first name (optional)
 * @param req.body.email: user's email (required)
 * @param req.body.password: user's password (required)
 */
async function createUser(req, res) {
	try {
		const isExists = await userEntity.checkIsUserExists(req.body);
		if (isExists) throw new Error('User with email is already exists.');
		const result = await userEntity.createUser(req.body);
		const token = await jwtService.createToken({ 'userId': result._id, 'email': result.email });
		const dataToSave = { 'userId': result._id, 'email': result.email };
		redisClient.storeValue(result._id.toString(), JSON.stringify(dataToSave));
		result.token = token;
		return apiResponder(res, 'User created successfully!', 201, (_.pick(result, '_id', 'firstName', 'lastName', 'email', 'token')));
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

/**
 * @function loginUser
 * @description to login an user
 * @author Rajat Maheshwari
 * @param req.body.email: user's email (required)
 * @param req.body.password: user's password (required)
 */
async function loginUser(req, res) {
	try {
		const step1 = await userEntity.checkIsUserExists(req.body);
		if (!step1) throw new Error('Please enter your registered email address.');
		const isPasswordMatched = await matchPassword(req.body.password, step1.hash, step1.salt);
		if (!isPasswordMatched) throw new Error('Please enter a valid password.');

		const token = await jwtService.createToken({ 'userId': step1._id, 'email': step1.email });
		const dataToSave = { 'userId': step1._id, 'email': step1.email };
		redisClient.storeValue(step1._id.toString(), JSON.stringify(dataToSave));
		step1.token = token;
		return apiResponder(res, 'User logged in successfully!', 200, (_.pick(step1, '_id', 'firstName', 'lastName', 'email', 'token')));
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

/**
 * @function userDetails
 * @description to get user details
 * @author Rajat Maheshwari
 * @param req.params.userId: user's unique id (required)
 */
async function userDetails(req, res) {
	try {
		const result = await userEntity.findUserById(req.user.userId);
		if (!result) throw new Error('User does not exists.');
		return apiResponder(res, 'User details get successfully!', 200, (_.pick(result, '_id', 'email', 'firstName', 'lastName', 'userType', 'subjects')));
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

/**
 * @function userLogout
 * @description to logout an user
 * @author Rajat Maheshwari
 */
async function userLogout(req, res) {
	try {
		redisClient.deleteKey(req.user.userId.toString());
		return apiResponder(res, 'User logged out successfully!', 200, {});
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

module.exports = {
	createUser,
	loginUser,
	userDetails,
	userLogout
};