const apiResponder = require('../util/responseHandler').apiResponder;
const roomEntity = require('../entity/room');

/**
 * @function chatList
 * @description to list all chats
 * @author Rajat Maheshwari
 * @param req.query.pageNo: given page no (required)
 * @param req.query.limit: given limit (required)
 */
async function chatList(req, res) {
	try {
		const data = await roomEntity.chatList(req.query, req.user.userId);
		return apiResponder(res, 'Chat list get successfully!', 200, data);
	} catch (error) {
		return apiResponder(res, error.message, 400, error);
	}
}

module.exports = {
	chatList
};