const cors = require('cors');
const express = require('express');
const fs = require('fs');
const mongoose = require('mongoose');

const { SERVER } = require('./lib/config/index');

const app = express();

app.use(express.json());

app.use(function (request, response, next) {
	console.log('--------------------------------REQUEST STARTS----------------------------------------');
	console.log(request.originalUrl);
	console.log('Request Address=======>', request.headers['x-forwarded-for'] || request.connection.remoteAddress);
	console.log('Request Type=======>', request.method.toUpperCase());
	console.log('Request Path=======>', request.path);
	console.log('Request Body=======>', request.body);
	console.log('Request Params=====>', request.params);
	console.log('Request Query======>', request.query);
	console.log('Authorization======>', request.get('authorization'));
	console.log('--------------------------------REQUEST ENDS------------------------------------------');

	if (request.method === 'OPTIONS') {
		response.send(200);
	} else {
		next();
	}
});

// enable cors
app.use(cors({
	'allowedHeaders': ['api_key', 'application/x-www-form-urlencoded', 'application/json', 'Content-Type', 'authorization'],
	'exposedHeaders': ['api_key'],
	'origin': '*',
	'methods': 'GET,PUT,POST,DELETE',
	'preflightContinue': false
}));

// database logs
mongoose.set('debug', true);

// attach the routes to the app
require('./lib/route/index')(app);

// database connection
const dbUrl = SERVER.MONGO.DB_URL + SERVER.MONGO.DB_NAME;
mongoose.connect(dbUrl, SERVER.MONGO.OPTIONS).catch(error => {
	console.warn('Failed to connect to mongodb instance.');
});

// initialize socket
require('./lib/util/socket/socketIO');

// initialize redis client
require('./lib/util/redisClient');

app.listen(SERVER.PORT, () => {
	console.log(`app is running at ${SERVER.PORT}`);
});